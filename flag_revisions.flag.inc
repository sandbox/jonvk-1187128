<?php

/**
 * Implements a node flag.
 */
class flag_revisions_flag extends flag_node {
  function _load_content($content_id) {
    $nid = db_result(db_query("SELECT nid FROM {node_revisions} WHERE vid = %d", $content_id));
    return is_numeric($content_id) ? node_load($nid, $vid) : NULL;
  }

  function get_content_id($node) {
    return $node->vid;
  }

  function access_multiple($content_ids, $account = NULL) {
    // flag_flag code
    $account = isset($account) ? $account : $GLOBALS['user'];
    $access = array();

    // First check basic user access for this action.
    foreach ($content_ids as $content_id => $action) {
      $access[$content_id] = $this->user_access($content_ids[$content_id], $account);
    }

    // Merge in module-defined access.
    foreach (module_implements('flag_access_multiple') as $module) {
      $module_access = module_invoke($module, 'flag_access_multiple', $this, $content_ids, $account);
      foreach ($module_access as $content_id => $content_access) {
        if (isset($content_access)) {
          $access[$content_id] = $content_access;
        }
      }
    }


    // Ensure that only flaggable node types are granted access. This avoids a
    // node_load() on every type, usually done by applies_to_content_id().
    $vids = implode(',', array_map('intval', array_keys($content_ids)));
    $placeholders = implode(',', array_fill(0, sizeof($this->types), "'%s'"));
    $result = db_query("SELECT vid as content_id FROM {node} WHERE vid IN ($vids) AND type NOT IN ($placeholders)", $this->types);
    while ($row = db_fetch_object($result)) {
      $access[$row->content_id] = FALSE;
    }

    return $access;
  }

  function get_views_info() {
    return array(
      'views table' => 'node_revisions',
      'join field' => 'vid',
      'title field' => 'title',
      'title' => t('Node revision flag'),
      'help' => t('Limit results to only those node revisions flagged by a certain flag; Or display information about the flag set on a node.'),
      'counter title' => t('Node flag counter'),
      'counter help' => t('Include this to gain access to the flag counter field.'),
    );
  }

  /**
   * No support for translations: revisions should be flagged individually in each language.
   */
  function get_translation_id($content_id) {
    return $content_id;
  }
}